<?php

// Class loader configuration - don't use if you have your own

use Papi\Thrift\Model\TUserLikeIds;
use Papi\Thrift\TPapiServiceClient;

require_once "Thrift/ClassLoader/ThriftClassLoader.php";
$loader = new \Thrift\ClassLoader\ThriftClassLoader();
$loader->registerNamespace("Thrift", __DIR__);
$loader->registerNamespace("Papi", __DIR__);
$loader->register();
// End of class loader configuration

const TIMEOUT = 60000;

$transport = new TSocket("api.applymagicsauce.com", "9901");
$transport->setRecvTimeout(TIMEOUT);
$transport->setSendTimeout(TIMEOUT);
$papiClient = new TPapiServiceClient(new TCompactProtocol($transport));
$transport->open();

// This is what you got during registration
$customerId = 1;
$apiKey = "apiKey";

// Now lets create some users with like ids
$userLikeIdsList = array();
for ($i = 0; $i < 10; $i++) {
    // This structure holds information about single user
    $userLikeIds = new TUserLikeIds();
    // Facebook id of user
    $userLikeIds->uid = $i;
    // Like ids of user. This structure may look wierd, but PHP does not support sets,
    // that's why Thrift simulates it by interpreting array keys as set members, while
    // values are irrelevant.
    $userLikeIds->likeIds = array(
        7010901522 => 0,
        7721750727 => 0,
        7557552517 => 0,
        8536905548 => 0,
        7723400562 => 0,
        8800570812 => 0,
        10765693196 => 0,
        14269799090 => 0,
        12938634034 => 0,
        14287253499 => 0);
    $userLikeIdsList[] = $userLikeIds;
}

// Authentication. This token will be valid for at least one hour, so you want to store it
// and re-use for further requests
$token = $papiClient->requestToken($customerId, $apiKey);

// Gets predictions
$predictionList = $papiClient->getBatchByLikeIds(
    array("ope", "con", "ext", "agr", "neu"), $token->tokenString, $userLikeIdsList);

print_r($predictionList);