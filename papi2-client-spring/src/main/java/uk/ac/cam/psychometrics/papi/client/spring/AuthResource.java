package uk.ac.cam.psychometrics.papi.client.spring;

import uk.ac.cam.psychometrics.papi.client.spring.model.PapiToken;

public interface AuthResource {

    PapiToken requestToken(int customerId, String apiKey);

}
