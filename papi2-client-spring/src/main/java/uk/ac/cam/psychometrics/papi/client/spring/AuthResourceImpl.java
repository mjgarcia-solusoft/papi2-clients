package uk.ac.cam.psychometrics.papi.client.spring;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import uk.ac.cam.psychometrics.papi.client.spring.exception.NotAuthenticatedException;
import uk.ac.cam.psychometrics.papi.client.spring.exception.PapiClientException;
import uk.ac.cam.psychometrics.papi.client.spring.model.PapiToken;

class AuthResourceImpl implements AuthResource, Resource {

    // @VisibleForTesting
    static class AuthForm {
        @JsonProperty("customer_id")
        private final int customerId;
        @JsonProperty("api_key")
        private final String apiKey;

        AuthForm(int customerId, String apiKey) {
            this.customerId = customerId;
            this.apiKey = apiKey;
        }

        public int getCustomerId() {
            return customerId;
        }

        public String getApiKey() {
            return apiKey;
        }
    }

    private final RestTemplate restTemplate;
    private final String resourceUrl;

    AuthResourceImpl(RestTemplate restTemplate, String resourceUrl) {
        this.restTemplate = restTemplate;
        this.resourceUrl = resourceUrl;
    }

    @Override
    public PapiToken requestToken(int customerId, String apiKey) {
        try {
            return restTemplate.postForEntity(resourceUrl, createRequestEntity(customerId, apiKey), PapiToken.class).getBody();
        } catch (HttpStatusCodeException e) {
            handleError(e, customerId);
        } catch (Exception e) {
            throw new PapiClientException(String.format("Unexpected exception occurred on request to url=%s", resourceUrl), e);
        }
        return null;
    }

    private void handleError(HttpStatusCodeException e, int customerId) {
        String message = String.format("Service returned status code %d for customerId=%d, errorMessage=%s",
                e.getStatusCode().value(), customerId, e.getResponseBodyAsString());
        if (e.getStatusCode() == HttpStatus.UNAUTHORIZED) {
            throw new NotAuthenticatedException(message, e);
        } else {
            throw new PapiClientException(message, e);
        }
    }

    private HttpEntity<AuthForm> createRequestEntity(int customerId, String apiKey) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(new AuthForm(customerId, apiKey), headers);
    }

    @Override
    public boolean isConfigured() {
        return restTemplate != null && resourceUrl != null && !resourceUrl.isEmpty();
    }

}