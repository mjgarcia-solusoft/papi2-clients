package uk.ac.cam.psychometrics.papi.client.jersey;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import uk.ac.cam.psychometrics.papi.client.jersey.exception.NotAuthenticatedException;
import uk.ac.cam.psychometrics.papi.client.jersey.exception.PapiClientException;
import uk.ac.cam.psychometrics.papi.client.jersey.model.PapiToken;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthResourceImplTest {

    private static final String RESOURCE_URL = "http://localhost/auth";
    private static final int CUSTOMER_ID = 1;
    private static final String API_KEY = "apiKey";
    private static final String BODY = String.format("{ \"customer_id\": %d, \"api_key\": \"%s\" }", CUSTOMER_ID, API_KEY);
    private static final String TOKEN_STRING = "token";

    @Mock
    private Client client;

    @Mock
    private Response response;

    @Spy
    private final AuthResourceImpl authResource = new AuthResourceImpl(client, RESOURCE_URL);

    @Test
    public void shouldReturnPapiTokenWhenRequestTokenReceivedStatusOk() {
        doReturn(response).when(authResource).getRequestTokenResponse(RESOURCE_URL, BODY);
        when(response.getStatus()).thenReturn(200);
        when(response.readEntity(PapiToken.class)).thenReturn(createPapiToken());
        PapiToken token = authResource.requestToken(CUSTOMER_ID, API_KEY);
        assertEquals(TOKEN_STRING, token.getTokenString());
        assertTrue(token.getExpires() > System.currentTimeMillis());
    }

    private PapiToken createPapiToken() {
        return new PapiToken(TOKEN_STRING, System.currentTimeMillis() + 3600000);
    }

    @Test
    public void shouldThrowExceptionWhenRequestTokenReceivedUnauthorizedStatus() {
        doReturn(response).when(authResource).getRequestTokenResponse(anyString(), anyString());
        when(response.getStatus()).thenReturn(401);
        try {
            authResource.requestToken(CUSTOMER_ID, API_KEY);
            fail("Was expecting exception");
        } catch (NotAuthenticatedException expected) {
        }
    }

    @Test
    public void shouldThrowExceptionWhenRequestTokenReceivedUnknownStatus() {
        doReturn(response).when(authResource).getRequestTokenResponse(anyString(), anyString());
        when(response.getStatus()).thenReturn(666);
        try {
            authResource.requestToken(CUSTOMER_ID, API_KEY);
            fail("Was expecting exception");
        } catch (PapiClientException expected) {
        }
    }
}
